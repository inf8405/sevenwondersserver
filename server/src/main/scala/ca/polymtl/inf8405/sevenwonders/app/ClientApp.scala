package ca.polymtl.inf8405.sevenwonders
package app

import api._

import org.apache.thrift._
import protocol.TBinaryProtocol
import transport.TSocket
import api.HelloService.Client


object ClientApp extends App 
{
  val transport = new TSocket("localhost", 7911)
  val protocol = new TBinaryProtocol(transport)
  val client = new Client(protocol)

  transport.open()

  println( client.sayHello( new HelloMsg("hi") ) )

  transport.close()

}