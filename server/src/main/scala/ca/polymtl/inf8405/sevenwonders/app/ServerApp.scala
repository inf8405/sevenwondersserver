package ca.polymtl.inf8405.sevenwonders
package app

import api._

import org.apache.thrift._
import transport.TServerSocket
import server.TThreadPoolServer

object ServerApp extends App 
{
	private def start()
	{
		val serverTransport = new TServerSocket(7911)
		val processor = new HelloService.Processor(new HelloServiceImpl())
		val server = new TThreadPoolServer( 
			new TThreadPoolServer.Args(serverTransport).processor(processor) 
		)

		server.serve()
	}

	class HelloServiceImpl extends HelloService.Iface
	{
		def sayHello( msg: HelloMsg ) = s"hello $msg"
	}

	start()
}
