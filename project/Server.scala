import sbt._
import Keys._

import com.github.bigtoast.sbtthrift.ThriftPlugin

object Settings
{
  val scalaVersion = "2.10.0"
  val version = "0.1-SNAPSHOT"
  val org = "ca.polymtl.inf8405"
  val resolvers = Seq(
    "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases",
    "Typesafe" at "http://typesafe.artifactoryonline.com/typesafe/repo"
  )
}

object Server extends Build {

  import Depedencies._

  lazy val server = Project(
    id = "Server",
    base = file("server"),
    settings = Project.defaultSettings ++ Seq(
      name := "Server",
      organization := Settings.org,
      version := Settings.version,
      scalaVersion := Settings.scalaVersion,
      resolvers ++= Settings.resolvers,
      libraryDependencies += logback
    )
  ) dependsOn ( model, api )

  lazy val model = Project(
    id = "model",
    base = file("model")
  ) // dependsOn multiset via publish local !

  lazy val api = Project(
    id = "Api",
    base = file("api"),
    settings = Project.defaultSettings ++ ThriftPlugin.thriftSettings ++ Seq(
      name := "Api",
      organization := Settings.org,
      version := "0.1-SNAPSHOT",
      scalaVersion := Settings.scalaVersion,
      resolvers ++= Settings.resolvers,
      libraryDependencies += thrift
    )
  )
}