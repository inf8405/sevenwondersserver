import sbt._
import Keys._

object Depedencies
{
  lazy val thrift   = "org.apache.thrift" % "libthrift"         % "0.9.0"
  lazy val logback  = "ch.qos.logback"    % "logback-classic"   % "1.0.9"
}