import sbt._

import Keys._
import AndroidKeys._

object General 
{
  import Depedencies._

  val settings = Defaults.defaultSettings ++ Seq(
    name := "SevenWonders",
    organization := Settings.org,
    version := Settings.version,
    versionCode := 0,
    scalaVersion := Settings.scalaVersion,
    platformName in Android := "android-13",
    libraryDependencies += logback
  )

  val proguardSettings = Seq(
    useProguard in Android := false
  )

  lazy val fullAndroidSettings =
    General.settings ++
    AndroidProject.androidSettings ++
    TypedResources.settings ++
    proguardSettings ++
    AndroidManifestGenerator.settings ++
    AndroidMarketPublish.settings ++ Seq(
      keyalias in Android := "change-me"
    )
}

object AndroidBuild extends Build 
{
  lazy val base = "client"
  lazy val main = Project(
    "Client",
    file( base ),
    settings = General.fullAndroidSettings
  ) dependsOn Server.api

  lazy val tests = Project(
    "ClientTests",
    file( base + "/tests" ),
    settings = General.settings ++
               AndroidTest.androidSettings ++
               General.proguardSettings ++ Seq (
      name := "Client Tests"
    )
  ) dependsOn main
}
