namespace java ca.polymtl.inf8405.sevenwonders.api

struct HelloMsg {
  1: required string name;
}

service HelloService {
  string sayHello(1:HelloMsg msg)
}